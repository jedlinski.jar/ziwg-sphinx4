/**
 * Created by szef on 15.03.17.
 */

import static java.lang.System.out;

import java.util.logging.Logger;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;


public class SpeechRecognizer {

	public static void main(String[] args) throws Exception {

		Configuration configuration = new Configuration();

		configuration.setDictionaryPath("resource:/pl/dictionary.dict");
		configuration.setLanguageModelPath("resource:/pl/language-model.lm");
		configuration.setAcousticModelPath("resource:/pl/acoustic");
		configuration.setSampleRate(16000);

		//ustawienia gramatyki
		configuration.setUseGrammar(true);
		configuration.setGrammarName("sentences2");
		configuration.setGrammarPath("resource:/pl/");

		disableLogs();

		LiveSpeechRecognizer recognizer = new LiveSpeechRecognizer(configuration);
		//StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);

		//InputStream stream = new FileInputStream(new File("sample1.wav"));

		//recognizer.startRecognition(stream);
		recognizer.startRecognition(true);

		SpeechResult result;
		out.println("Ready");
		while ((result = recognizer.getResult()) != null) {

			String hypothesis = result.getHypothesis();

			if (hypothesis == null || hypothesis.isEmpty()) {
				continue;
			}
			out.format("Hypothesis: %s\n", hypothesis);

			//Słabo działa, długi czas obliczania score'a, a confidence jeszcze nie zaimplementowane (zawsze zwraca 0.0)
			/*
			recognizer.getResult().getWords()
					.forEach(word -> out.println(word.getPronunciation() + " --- " + word.getScore()));
					*/
		}

		/*while (true) {

			ofNullable(recognizer.getResult())
					.map(SpeechResult::getHypothesis)
					.map(hypo -> "Said: " + hypo)
					.ifPresent(System.out::println);
		}*/
		//recognizer.stopRecognition();
	}

	private static void disableLogs() {
		Logger cmRootLogger = Logger.getLogger("default.config");
		cmRootLogger.setLevel(java.util.logging.Level.OFF);
		String conFile = System.getProperty("java.util.logging.config.file");
		if (conFile == null) {
			System.setProperty("java.util.logging.config.file", "ignoreAllSphinx4LoggingOutput");
		}
	}
}
